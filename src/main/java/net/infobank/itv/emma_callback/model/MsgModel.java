package net.infobank.itv.emma_callback.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class MsgModel implements Serializable{

	private String key;
	private String msg_type; 
	private String moRecipient;
	private String moOriginator;
	private String moCallback;
	private String subject;
	private String msg;
	private int carrier;
	private long date;
	private String attachFileList;   
    
	private String msg_com = "001";
}
