package net.infobank.itv.emma_callback;

import java.util.ArrayList;

import com.google.gson.Gson;

import ib.emma.callback.IBMMOCallback;
import net.infobank.itv.emma_callback.kafka.SimpleKafkaProducer;
import net.infobank.itv.emma_callback.model.MsgModel;

public class itvMMOCallback implements IBMMOCallback {
	
	
    public void doAfterDeque(int dbIndex, String key, String moRecipient,
            String moOriginator, String moCallback, String subject, String msg,
            int carrier, long date, ArrayList attachFileList) {
	
	    try{
	            String msg_type = "MMO";
	            final String topicName = moRecipient.replace("#", "");
	            
	    	    String szImg= "";
	    	    int num = attachFileList.size();
	    	
	    	    for ( int i =0 ; i < num ; i ++ )
	    	    {
	    	            String fname = (String) attachFileList.get(i);
	    	            fname = fname.toLowerCase();
	    	
	    	            if( fname.endsWith(".jpeg") || fname.endsWith(".jpg") || fname.endsWith(".gif") || fname.endsWith(".png") || fname.endsWith(".bmp") || fname.endsWith(".avi") || fname.endsWith(".asf") || fname.endsWith(".sktm") )
	    	            {
	    	                    if( szImg.length() > 0 )
	    	                            szImg += "|";
	    	                    szImg += (String) attachFileList.get(i);
	    	                    System.out.println(szImg);
	    	            }
	    	    }
	            
	            // Get Producer instance in try with resource to get it closed automatically
	            final SimpleKafkaProducer simpleKafkaProducer = SimpleKafkaProducer.getInstance();
	            
	            MsgModel msgmodel = new MsgModel();
	            
	            msgmodel.setKey(key);
	            msgmodel.setMsg_type(msg_type);
	            msgmodel.setMoRecipient(moRecipient);
	            msgmodel.setMoOriginator(moOriginator);
	            msgmodel.setMoCallback(moCallback);
	            msgmodel.setSubject(subject);
	            msgmodel.setMsg(msg.replaceAll("\\u0000",""));
	            msgmodel.setCarrier(carrier);
	            msgmodel.setAttachFileList(szImg);;
	            msgmodel.setDate(date);
	            
	            Gson g = new Gson();
	            String jsonString = g.toJson(msgmodel);
	            System.out.println("Sending messages to Kafka. : " + jsonString);
	            
	            simpleKafkaProducer.send(topicName, jsonString);
	            simpleKafkaProducer.close();
	            
	            
	    } catch(Exception e) {
	            e.printStackTrace();
	    }
    }

	@Override
    public void doAfterReceive(String key, String moRecipient,
            String moOriginator, String moCallback, String subject, String msg,
            int carrier, long date) {
        System.out.println("SampleMMOCallback doAfterReceive: " + moRecipient);

    }

}
