package net.infobank.itv.emma_callback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import ib.emma.callback.IBSMOCallback;
import net.infobank.itv.emma_callback.kafka.SimpleKafkaProducer;
import net.infobank.itv.emma_callback.model.MsgModel;

public class itvSMOCallback implements IBSMOCallback {
    private static Logger log = LoggerFactory.getLogger(itvSMOCallback.class);
    /**
     * SMSMO 를 수신하여 SMSMO 큐에 enqueue 후 호출되는 메소드
     *
     * @param key
     *            MO Key
     * @param moRecipient
     *            MO 특번
     * @param moOriginator
     *            MO 발신번호
     * @param moCallback
     *            MO 발신 시 발신자가 입력한 번호
     * @param msg
     *            메시지 내용
     * @param carrier
     *            최종착신망
     * @param date
     *            MO 발생 시간
     */
    public void doAfterReceive(String key, String moRecipient,
            String moOriginator, String moCallback, String msg, int carrier,
            long date, int extraValue) {

    	SimpleKafkaProducer simpleKafkaProducer = null;
        try{
        	
            log.info("SampleSMOCallback doAfterReceive: "+ moRecipient);
            String msg_type = "SMO";
            
            String topicName = moRecipient.replace("#", "");
            log.info("Topic Name: "+ topicName);

            // Get Producer instance in try with resource to get it closed automatically
            simpleKafkaProducer = SimpleKafkaProducer.getInstance();
            
            MsgModel msgmodel = new MsgModel();
            
            msgmodel.setKey(key);
            msgmodel.setMsg_type(msg_type);
            msgmodel.setMoRecipient(moRecipient);
            msgmodel.setMoOriginator(moOriginator);
            msgmodel.setMoCallback(moCallback);
            msgmodel.setMsg(msg.replaceAll("\\u0000",""));
            msgmodel.setCarrier(carrier);
            msgmodel.setDate(date);
            
            Gson g = new Gson();
            String jsonString = g.toJson(msgmodel);
            log.info("Sending messages to Kafka. : " + jsonString);
            
            simpleKafkaProducer.send(topicName, jsonString); 

        } catch(Exception e) {
        	log.info("Exception : ", e);
        }


    }

	@Override
    public void doAfterDeque(int dbIndex, String key, String moRecipient,
            String moOriginator, String moCallback, String msg, int carrier,
            long date, int extraValue) {
	    log.info("SampleSMOCallback doAfterDeque: "+ moRecipient);
		
	}

}