package net.infobank.itv.emma_callback;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmmaCallbackApplication {

	public static void main(String[] args) {
        String key = "key";
        String moRecipient = "#36818999";
        String moOriginator = "01027321830";
        String moCallback = "01027321830";
        String msg = "MSG";
        int carrier = 0;
        long date = 1234L;
        int extraValue  = 1;
        
	    itvSMOCallback callback = new itvSMOCallback();
	    
	    callback.doAfterReceive(key, moRecipient, moOriginator, moCallback, msg, carrier, date, extraValue);
	}

}
