package net.infobank.itv.emma_callback.kafka;

import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <code>{@link SimpleKafkaProducer}</code> exposes methods to send messages to Kafka. It is implemented based on
 * Singleton to avoid creation of multiple {@link KafkaProducer} instances.
 */
public class SimpleKafkaProducer {
    private static Logger log = LoggerFactory.getLogger(SimpleKafkaProducer.class);
    
	public static SimpleKafkaProducer INSTANCE = null;
	
    private final Producer<String, String> producer;
    
    
	private SimpleKafkaProducer() {
        // Set some properties, ideally these would come from properties file
        final Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "172.16.0.151:9092,172.16.0.152:9092,172.16.0.153:9092"); // Kafka brokers in format host1:port1,host2:port2
        props.put(ProducerConfig.ACKS_CONFIG, "1"); // 0 for no acknowledgements, 1 for leader acknowledgement and -1 for all replica acknowledgements
        props.put(ProducerConfig.LINGER_MS_CONFIG, "1"); // Frequency of message commits to Kafka
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        producer = new KafkaProducer<>(props);
    }

    
    /**
     * Factory method to get instance.
     *
     * @return instance of {@link SimpleKafkaProducer}
     */
    public static SimpleKafkaProducer getInstance() {
        if(INSTANCE == null){
            synchronized (SimpleKafkaProducer.class){
                if(INSTANCE == null){
                	INSTANCE = new SimpleKafkaProducer();
                }
            }
         }
        return INSTANCE;
    }

    /**
     * Sends message with input key and value to specified topic name.
     *
     * @param topicName name of topic to publish messages to
     * @param key key of message
     * @param value payload of message
     */
//    public RecordMetadata send(String topicName, String value) {
//    	RecordMetadata result = null;
//    	try { 
//    		Future<RecordMetadata> recordMetadata = producer.send(new ProducerRecord<>(topicName, value));
//			result = recordMetadata.get();
//    	} catch (KafkaException e) {
//    		System.out.println("KafkaException");
//        	e.printStackTrace();
//            producer.abortTransaction();
//        } catch (InterruptedException e) {
//			System.out.println("InterruptedException");
//			e.printStackTrace();
//		} catch (ExecutionException e) {
//			System.out.println("ExecutionException");
//			e.printStackTrace();
//		}
//    	return result;
//    }

    public void send(String topicName, String value) {
         ProducerRecord<String, String> producerRecord = new ProducerRecord<String, String>(topicName, value);
         
         producer.send(producerRecord, new Callback() {
             
             @Override
             public void onCompletion(RecordMetadata metadata, Exception e) {
                 
                 if(e != null) {
                	 log.info("Exception : ", e);
                 }else {
                     /*
                      * 정상처리 로직
                      */
                 }
                 
             }
         });

	}
    /**
     * Releases pool of buffer space that holds records that haven't yet been transmitted to the server as well as a
     * background I/O thread that is responsible for turning these records into requests and transmitting them to
     * the cluster.
     */
    public void close() {
    	if(producer != null) {
    		producer.close();
    	}
    }
}